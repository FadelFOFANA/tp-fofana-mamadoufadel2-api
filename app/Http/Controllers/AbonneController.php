<?php

namespace App\Http\Controllers;

use App\Models\Abonne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class AbonneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'hasError' => false,
            "message"=> "Liste des Abonnés",
            'data' => Abonne::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "nom" => "required",
            "prenom"=> "required",
            "email"=> "required|unique:abonnes",
            "active"=> "required",
        ]);

        if ($validator->fails()) {
            return response()->json([
                'hasError' => true,
                "message"=> "Une erreur est survenue",
                'data' => $validator->errors()->all()
            ]);
        }

        $request->validate([
            "nom" => 'required',
            "prenom"=> "required",
            "email"=> "required|unique:abonnes",
            "active"=> "required",
        ]);

        $abonne = Abonne::create([
            "nom" => $request->get('nom'),
            "prenom"=> $request->get('prenom'),
            "email"=> $request->get('email'),
            "contact"=> $request->get('contact'),
            "active"=> $request->get('active'),
        ]);

        return response()->json([
            'hasError' => false,
            "message"=> "Abonne ajoute avec succes",
            'data' => $abonne 
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Abonne  $abonne
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $abonne = Abonne::where('id', $id)->first();
        if ($abonne == null) {
            return response()->json([
                'hasError' => true,
                "message"=> "Une erreur est survenue! l'abonne avec ID :".$id . " n'existe pas" 
            ]);
        }
        return response()->json([
            'hasError' => false,
            "message"=> "Detail sur l'abonne",
            'data' => $abonne
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Abonne  $abonne
     * @return \Illuminate\Http\Response
     */
    public function edit(Abonne $abonne)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Abonne  $abonne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $abonne = Abonne::where('id', $id)->first();
        if($abonne == null){
            return response()->json([
                'hasError' => true,
                'message' => 'Une erreur est survenu lors du traitement : Abonne id = '.$id.' not 
                exist',
            ]);
        }
        $validator = Validator::make($request->all(), [
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|unique:abonnes,id,'.$abonne->id,
            'active' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'hasError' => true,
                'message' => 'Une erreur est survenu lors du traitement',
                'data' => $validator->errors()->all()
            ]);
        }
        $abonne->update([
            'nom' => $request->get('nom'),
            'prenom' => $request->get('prenom'),
            'email' => $request->get('email'),
            'active' => $request->get('active')
        ]);

        return response()->json([
                'hasError' => false,
                'message' => "Abonne modifié avec succès",
                'data' => $abonne
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Abonne  $abonne
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $abonne = Abonne::where('id', $id)->first();
        if ($abonne == null) {
            return response()->json([
                'hasError' => true,
                "message"=> "Une erreur est survenue! l'abonné avec ID :".$id . " n'existe pas" 
            ]);
        }

        $abonne->delete();
        return response()->json([
                'hasError' => false,
                "message"=> "Suppression effectué avec succès",
                'data' => null 
            ]);
    }

    public function getAllAbonnes()
    {
        $abonne = Abonne::with('comptes')->get();
        if ($abonne == null) {
            return response()->json([
                'hasError' => true,
                "message"=> "La Table Abonne est vide" 
            ]);
        }
        return response()->json([
                'hasError' => false,
                "message"=> "Liste des Abonnées avec leurs comptes",
                'data' => $abonne 
            ]);
    }

    public function showCompteById($id)
    {
        $abonne = Abonne::where('id', $id)->first();
        $abonneWithCompte = $abonne->comptes()->get();
        if ($abonneWithCompte == null) {
            return response()->json([
                'hasError' => true,
                "message"=> "Une erreur est survenue! l'abonne avec ID n'existe pas" 
            ]);
        }
        return response()->json([
                'hasError' => false,
                "message"=> "Detail sur Les COMPTES de l'abonné",
                'data' => $abonneWithCompte
            ]);
    }
    
}
