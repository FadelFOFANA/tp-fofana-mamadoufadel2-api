<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    public function register(Request $request): string{
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required' 
        ]);

        if($validator->fails()){
            return response()->json([
                'hasError' => true,
                'message' => 'Une erreur est survenue lors du traitement',
                'data' => $validator->errors()->all()
            ], 422);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'hasError' => false,
            'message' => 'Utilisateur cree avec succes',
            'data' => $user ], 200);
    }
    /**
     * Login The User
    * @param Request $request
    * @return User
    */ 
    
    public function login(Request $request){
        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required' 
        ]);

        if($validator->fails()){
            return response()->json([
                'hasError' => true,
                'message' => 'Une erreur est survenue lors du traitement',
                'data' => $validator->errors()->all()
            ], 401);
        }

        if(!Auth::attempt($request->only(['email', 'password']))){
            return response()->json([
                'hasError' => true,
                'message' => 'Une erreur est survenue lors du traitement, login/password incorrect' 
            ], 401);
        }

        $user = User::where('email', $request->email)->first();
        return response()->json([
            'hasError' => false,
            'message' => 'Utilisateur authentifié avec succes',
            'data' => [
            'token' => $user->createToken("API TOKEN")->plainTextToken ]
        ], 200);
    }
}
