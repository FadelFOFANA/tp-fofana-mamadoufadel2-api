<?php

namespace App\Http\Controllers;

use App\Models\Abonne;
use App\Models\Compte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class LiaisonController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "abonneId" => "required",
            "compteId"=> "required",
        ]);

        if ($validator->fails()) {
            return response()->json([
                'hasError' => true,
                "message"=> "Une erreur est survenue",
                'data' => $validator->errors()->all()
            ]);
        }

        $request->validate([
            "abonneId" => "required",
            "compteId"=> "required",
        ]);

        $Compte = Compte::where('id', $request->get('compteId'))->first();
        if($Compte == null){
            return response()->json([
                    'hasError' => true,
                    'message' => 'Une erreur est survenu lors du traitement : Compte id = '.$request->get('compteId').' not exist',
                ]);
        }

        $abonne = Abonne::where('id', $request->get('abonneId'))->first();
        if($abonne == null){
            return response()->json([
                'hasError' => true,
                'message' => 'Une erreur est survenu lors du traitement : Abonné id = '.$request->get('abonneId').' not exist',
            ]);
        }

        $Compte->update([
            "abonne_id" => $request->get('abonneId'),
        ]);
    
        return response()->json([
            'hasError' => false,
            "message"=> "La Liaison a été ajouté avec succès",
            'data' => null 
        ]);
    }
}