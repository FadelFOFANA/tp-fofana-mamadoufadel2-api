<?php

namespace App\Http\Controllers;

use App\Models\Abonne;
use App\Models\Personne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PersonneController extends Controller
{

    public function index()
    {
        $response = Http::get('http://pplapi.com/random.json');
        $responsex = Http::get('https://rawcdn.githack.com/kamikazechaser/administrative-divisions-db/master/api/'.
        strtoupper($response['country_tld']) .'.json');
        return response()->json([
            'langue' => $response['language'],
            'genre' => $response['sex'] == 'Male' ? 'Masculin': 'Feminin',
            'religion' => $response['religion'],
            'pays' => $response['country_name'],
            'indicatif' => strtoupper($response['country_tld']),
            'internet' => $response['internet'],
            'regions' => $responsex->json()
        ]);
    }
}
