<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Abonne extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    
    public function comptes(){
        return $this->hasMany(Compte::class);
    }
}
