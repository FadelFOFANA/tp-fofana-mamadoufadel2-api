<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Compte>
 */
class CompteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'libelle' => fake()->title(),
            'description' => 'MY DESCRIPTION '. fake()->randomNumber(),
            'agence' => Str::random(5),
            'banque' => Str::random(5),
            'numero' => rand(10000000000, 99999999999),
            'rib' => rand(10, 99),
            'montant' => rand(500, 999999999),
            'domiciliation' => fake()->country(),
        ];
    }
}
