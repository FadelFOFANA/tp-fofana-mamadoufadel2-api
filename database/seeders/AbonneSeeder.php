<?php

namespace Database\Seeders;

use App\Models\Abonne;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AbonneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Abonne::factory()
            ->count(10)
            ->hasComptes(3)
            ->create();
    }
}
