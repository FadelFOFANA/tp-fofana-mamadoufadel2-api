<?php

use App\Http\Controllers\AbonneController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CompteController;
use App\Http\Controllers\LiaisonController;
use App\Http\Controllers\PersonneController;
use App\Http\Controllers\StatistiqueController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/v1')->group(function () {
    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/abonnes/comptes', [AbonneController::class, 'getAllAbonnes']);
        Route::apiResource("/abonnes", AbonneController::class);
        Route::apiResource("/comptes", CompteController::class);
        Route::get('/abonnes/{id}/comptes', [AbonneController::class, 'showCompteById']);
        Route::post('/liaison', [LiaisonController::class, 'store']);
        Route::get('/stats', [StatistiqueController::class, 'index']);
        Route::get('/stats/abonnes/{id}', [StatistiqueController::class, 'statsByAbonneId']);
        Route::get('/personnes/random', [PersonneController::class, 'index']);
    });
    Route::post('/register',[AuthController::class, 'register']);
    Route::get('/login', [AuthController::class, 'login']);
});